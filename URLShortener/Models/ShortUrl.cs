﻿using System.ComponentModel.DataAnnotations;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace URLShortener.Models;

public class ShortUrl
{
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string? Id { get; set; }
    
    [BsonRequired]
    [BsonElement("Original")]
    public string Original { get; set; }
    
    [BsonRequired]
    [BsonElement("Shortened")]
    public string Shortened { get; set; }
}