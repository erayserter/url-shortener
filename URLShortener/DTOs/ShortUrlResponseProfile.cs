﻿using AutoMapper;
using URLShortener.Models;

namespace URLShortener.DTOs;

public class ShortUrlResponseProfile: Profile
{
    public ShortUrlResponseProfile()
    {
        CreateMap<ShortUrl, ShortUrlResponse>();
    }
}