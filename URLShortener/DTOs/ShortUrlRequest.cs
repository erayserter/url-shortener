﻿using ThirdParty.Json.LitJson;

namespace URLShortener.DTOs;

public class ShortUrlRequest
{
    public string Original { get; set; }
}