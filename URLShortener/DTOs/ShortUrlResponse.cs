﻿namespace URLShortener.DTOs;

public class ShortUrlResponse
{
    public string Original { get; set; }
    public string Shortened { get; set; }
}