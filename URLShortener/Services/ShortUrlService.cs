﻿using AutoMapper;
using URLShortener.DTOs;
using URLShortener.Helpers;
using URLShortener.Models;
using URLShortener.Repositories;

namespace URLShortener.Services;

public class ShortUrlService: IShortUrlService
{
    private readonly IShortUrlRepository<ShortUrl> _shortUrlRepository;
    private readonly IMapper _mapper;

    public ShortUrlService(IShortUrlRepository<ShortUrl> shortUrlRepository, IMapper mapper)
    {
        _shortUrlRepository = shortUrlRepository;
        _mapper = mapper;
    }

    public ShortUrlResponse GetShortUrlByOriginal(string original)
    {
        var shortUrl = _shortUrlRepository.Get(shortUrl => shortUrl.Original == original);
        return _mapper.Map<ShortUrlResponse>(shortUrl);
    }

    public ShortUrlResponse GetShortUrlByShortened(string shortened)
    {
        var shortUrl = _shortUrlRepository.Get(shortUrl => shortUrl.Shortened == shortened);
        return _mapper.Map<ShortUrlResponse>(shortUrl);
    }


    public ShortUrlResponse CreateShortUrl(ShortUrlRequest shortUrlRequest)
    {
        var shortUrl = _shortUrlRepository.Get(shortUrl => shortUrl.Original == shortUrlRequest.Original);

        if (shortUrl != null) return _mapper.Map<ShortUrlResponse>(shortUrl);
        
        shortUrl = new ShortUrl
        {
            Original = shortUrlRequest.Original,
            Shortened = ShortUrlHelper.RandomShortUrl()
        };
        
        _shortUrlRepository.Add(shortUrl);

        return _mapper.Map<ShortUrlResponse>(shortUrl);
    }

    public ShortUrlResponse RefreshShortUrl(string shortened)
    {
        var shortUrl = _shortUrlRepository.Get(url => url.Shortened == shortened);

        if (shortUrl == null) return null;

        shortUrl.Shortened = ShortUrlHelper.RandomShortUrl();
        
        _shortUrlRepository.Update(url => url.Shortened == shortened, shortUrl);

        return _mapper.Map<ShortUrlResponse>(shortUrl);
    }

    public void ExpireShortUrl(string shortened)
    {
        _shortUrlRepository.Delete(shortUrl => shortUrl.Shortened == shortened);
    }
}