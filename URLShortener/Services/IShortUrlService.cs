﻿using URLShortener.DTOs;
using URLShortener.Models;

namespace URLShortener.Services;

public interface IShortUrlService
{
    public ShortUrlResponse GetShortUrlByOriginal(string original);
    public ShortUrlResponse GetShortUrlByShortened(string shortened);
    public ShortUrlResponse CreateShortUrl(ShortUrlRequest shortUrlRequest);
    public ShortUrlResponse RefreshShortUrl(string shortened);
    public void ExpireShortUrl(string shortened);
}