﻿using System.Text;
using Microsoft.Extensions.Primitives;
using MongoDB.Driver.Core.WireProtocol.Messages;

namespace URLShortener.Helpers;

public class ShortUrlHelper
{

    private const string Language = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789";
    private const int UrlLength = 7;
    
    public static string RandomShortUrl()
    {
        var randomizer = new Random();
        var shortened = new StringBuilder();

        for (var i = 0; i < UrlLength; i++)
            shortened.Append(Language[randomizer.Next(0, Language.Length)]);

        return shortened.ToString();
    }
}