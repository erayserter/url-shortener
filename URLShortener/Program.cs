using URLShortener.Configurations;
using URLShortener.Models;
using URLShortener.Repositories;
using URLShortener.Services;

var builder = WebApplication.CreateBuilder(args);

// Mapper
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

// Configurations
builder.Services.Configure<DatabaseSettings>(builder.Configuration.GetSection("MongoDB"));

// Services
builder.Services.AddSingleton<IShortUrlRepository<ShortUrl>, MongoRepository<ShortUrl>>();
builder.Services.AddSingleton<IShortUrlService, ShortUrlService>();

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();