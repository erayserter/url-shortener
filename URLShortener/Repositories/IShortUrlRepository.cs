﻿using System.Linq.Expressions;
using MongoDB.Driver;

namespace URLShortener.Repositories;

public interface IShortUrlRepository<T>
{
    public List<T> GetAll(Expression<Func<T, bool>> filter);
    public T Get(Expression<Func<T, bool>> filter);
    public void Add(T model);
    public void Update(Expression<Func<T, bool>> filter, T model);
    public void Delete(Expression<Func<T, bool>> filter);
    public void DeleteMany(FilterDefinition<T> filter);
}