﻿using System.Linq.Expressions;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using URLShortener.Configurations;

namespace URLShortener.Repositories;

public class MongoRepository<T>: IShortUrlRepository<T>
{
    private readonly IMongoCollection<T> _mongoCollection;

    public MongoRepository(IOptions<DatabaseSettings> databaseSettings)
    {
        var mongoClient = new MongoClient(databaseSettings.Value.ConnectionString);
        var mongoDb = mongoClient.GetDatabase(databaseSettings.Value.DatabaseName);
        _mongoCollection = mongoDb.GetCollection<T>(typeof(T).Name);
    }
    
    public List<T> GetAll(Expression<Func<T, bool>> filter) => _mongoCollection.Find(filter).ToList();
    public T Get(Expression<Func<T, bool>> filter) => _mongoCollection.Find(filter).FirstOrDefault();
    public void Add(T model) => _mongoCollection.InsertOne(model);
    public void Update(Expression<Func<T, bool>> filter, T model) => _mongoCollection.FindOneAndReplace(filter, model);
    public void Delete(Expression<Func<T, bool>> filter) => _mongoCollection.FindOneAndDelete(filter);
    public void DeleteMany(FilterDefinition<T> filter) => _mongoCollection.DeleteMany(filter);
}