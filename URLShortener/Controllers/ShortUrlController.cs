﻿using Microsoft.AspNetCore.Mvc;
using URLShortener.DTOs;
using URLShortener.Services;

namespace URLShortener.Controllers;

[ApiController]
[Route("api/[controller]")]
public class ShortUrlController: ControllerBase
{
    private readonly IShortUrlService _shortUrlService;

    public ShortUrlController(IShortUrlService shortUrlService)
    {
        _shortUrlService = shortUrlService;
    }

    [HttpGet]
    public IActionResult GetShortUrl(
        [FromQuery] string? original,
        [FromQuery] string? shortened)
    {
        ShortUrlResponse shortUrlResponse = null;
        
        if (original != null)
            shortUrlResponse = _shortUrlService.GetShortUrlByOriginal(original);
        else if (shortened != null)
            shortUrlResponse = _shortUrlService.GetShortUrlByShortened(shortened);
        
        if (shortUrlResponse == null)
            return NotFound();

        return Ok(shortUrlResponse);
    }

    [HttpPost]
    public IActionResult CreateShortUrl(ShortUrlRequest shortUrlRequest)
    {
        var shortUrlResponse = _shortUrlService.CreateShortUrl(shortUrlRequest);
        return CreatedAtAction(nameof(this.GetShortUrl), shortUrlResponse);
    }

    [HttpDelete]
    public IActionResult DeleteShortUrl(string shortUrl)
    {
        _shortUrlService.ExpireShortUrl(shortUrl);
        return NoContent();
    }

    [HttpPut]
    public IActionResult UpdateShortUrl(string shortUrl)
    {
        var shortUrlResponse = _shortUrlService.RefreshShortUrl(shortUrl);
        return Ok(shortUrlResponse);
    }
}